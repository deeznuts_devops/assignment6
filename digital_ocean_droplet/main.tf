resource "digitalocean_droplet" "web" {
  image  = "ubuntu-20-04-x64"
  name   = "web-1"
  size   = "s-1vcpu-1gb"
  ssh_keys = [ var.ssh_key, ]

  provisioner "remote-exec" {
        inline = [
            "sudo useradd -m -s /bin/bash ansible",
            "sudo usermod -aG sudo ansible",
        ]
    }
  connection {
        type = "ssh"
        user = "root"
        host = digitalocean_droplet.web.ipv4_address
        private_key = file("~/.ssh/id_rsa")
    }
}