output "vm_ip" {
  value = digitalocean_droplet.web.ipv4_address
}