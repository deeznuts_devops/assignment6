#!/bin/bash

droplet_ip="123.123.23.12" # Well, can be any other IP addresss


ping -c 4 ${droplet_ip} > /dev/null


if [ $? -eq 0 ]; then
    echo "Droplet is reachable. Ping successful."
    exit 0
else
    echo "Droplet is not reachable. Ping failed."
    exit 1
fi
