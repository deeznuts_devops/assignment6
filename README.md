# Terraform Module: digitalocean-droplet

## Description

This Terraform module creates a new DigitalOcean droplet using the specified API key and SSH key fingerprint.
The droplet will be accessible via private ssh key pair from digital ocean.

## Usage

```hcl
module "my_droplet" {
  source              = "digitalocean-droplet"
  digital_ocean_api_key = "your_digitalocean_api_key"
  ssh_key  = "your_ssh_key_fingerprint"
}

Input parameters of the module

| Name          | Description        | Required |
| ------------- |:------------------:| -----:|
| digital_ocean_api_key  | An api key from digital ocean    | yes |
| ssh_key    | A fingerprint of ssh key from digital ocean |   yes |


Output parameters of the module

| Name          | Description        |
| ------------- |:------------------:|
| vm_ip  | IPv4 address of the created droplet |


TODO
Adjustments for the project:
1. Take more parameters to create more customizable droplet